# 540-Fall2023-Examples

Example code illustrating the use of code to control Raspberry Pi sensors & actuators.

## Getting started

To run, use `python filename.py`

## Documentation
Raspberry Pi Camera (Hardware): https://www.raspberrypi.com/documentation/accessories/camera.html#getting-started

Pi Camera2 (Python library): https://datasheets.raspberrypi.com/camera/picamera2-manual.pdf 